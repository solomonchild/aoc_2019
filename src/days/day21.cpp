#include <day.hpp>

#include <int_code.hpp>
#include <grid.hpp>

struct Day21 : public Solution {
    using Solution::Solution;
    Program master;
    void prepare(std::istream&& is) override {
        master = parse(std::move(is));
    }

    void push(CPU& cpu, const std::string& what) {
        for(auto c : what) 
            cpu.in_.push_back(c);
        cpu.in_.push_back('\xA');
    }


    std::string part1() override { 
        CPU cpu;
        auto local = master;
        auto reason = cpu.run(local);
        while(reason != InterruptReason::Halt) {
            if(reason == InterruptReason::Input) {
                push(cpu, "NOT C T"); 
                push(cpu, "NOT A J"); 
                push(cpu, "OR T J"); 

                push(cpu, "AND D J"); 

                push(cpu, "WALK");
            }
            if(reason == InterruptReason::Output) {
                std::cout << char(cpu.out_);
            }
            reason = cpu.run(local);
        }
        return std::to_string(cpu.out_);
    }

    std::string part2() override { 
        CPU cpu;
        auto local = master;
        auto reason = cpu.run(local);
        while(reason != InterruptReason::Halt) {
            if(reason == InterruptReason::Input) {

                //!(A&C&B) & D & (E&I || H || (E&F))
                push(cpu, "OR E T"); //E
                push(cpu, "AND I T"); //E&I
                push(cpu, "OR H T");  //E&I | H
                push(cpu, "OR T J");  //J = E&I|H

                push(cpu, "OR E T"); 
                push(cpu, "AND F T"); 
                push(cpu, "OR T J");  //J = (E&I || H || (E&F&G)

                // T=!(!A)=A
                push(cpu, "NOT A T"); 
                push(cpu, "NOT T T"); 

                push(cpu, "AND B T"); 
                push(cpu, "AND C T"); 
                push(cpu, "NOT T T"); 
                push(cpu, "AND D T"); 
                push(cpu, "AND T J"); 
                push(cpu, "RUN");
            }
            if(reason == InterruptReason::Output) {
                std::cout << char(cpu.out_);
            }
            reason = cpu.run(local);
        }
        return std::to_string(cpu.out_);
    }
};

template<>
void solve<Day::day21>(const std::string& ans1, const std::string& ans2) {
    Day21 d(Day::day21, false, ans1, ans2);
    d.solve();
}
