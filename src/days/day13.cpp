#include <day.hpp>

#include <cassert>
#include <grid.hpp>
#include <int_code.hpp>

struct Arcade : public CPU {
    std::vector<int> output_;
    InterruptReason run(Program& p) override {
        auto reason = CPU::run(p);
        if(reason == InterruptReason::Output) {
            output_.clear();
            output_.push_back(out_);
            assert(CPU::run(p) == InterruptReason::Output);
            output_.push_back(out_);
            assert(CPU::run(p) == InterruptReason::Output);
            output_.push_back(out_);
        }
        return reason;
    }
};
struct Day13 : public Solution {
    using Solution::Solution;
    Program master_program;
    enum TileId {
        Empty,
        Wall,
        Block,
        Paddle,
        Ball
    };

    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    void replace(vec2 coord, Grid& grid, int tile_id) {
        auto ch = " |=_o"[tile_id];
        assert(coord.y >= 0 && coord.y < grid.size() && coord.x >= 0 && coord.x < grid[0].size());
        grid[coord.y][coord.x] = ch;
    }

    std::string part1() override { 
        auto local_program = master_program;
        Arcade cpu;
        auto grid = cons_grid({100, 100}, ' ');
        static const auto init = std::numeric_limits<int>::min();
        int block_count{};
        while(true) {
            if(cpu.run(local_program) == InterruptReason::Halt) break;
            auto x = cpu.output_[0];
            auto y = cpu.output_[1];
            auto tile_id = cpu.output_[2];
            if(tile_id == TileId::Block) ++block_count;

            replace({x,y}, grid, tile_id);
        }
        return std::to_string(block_count);
    }
    inline static bool is_score(int x, int y) {
        return x == -1 && y == 0;
    }

    std::string part2() override {
        auto local_program = master_program;
        Arcade cpu;
        auto grid = cons_grid({40, 25}, ' ');
        local_program.code[0] = 2;

        auto score = 0;
        vec2 paddle_pos = vec2::null();
        vec2 ball_pos = vec2::null();

        std::ios::sync_with_stdio(false);
        constexpr bool SHOULD_PRINT = false;
        while(true) {
            auto opc = cpu.run(local_program);
            if(opc == InterruptReason::Halt) break;

            if(opc == InterruptReason::Output) {
                auto x = cpu.output_[0];
                auto y = cpu.output_[1];
                auto third = cpu.output_[2];

                if(is_score(x, y)) {
                    score = third;
                    continue;
                }

                if(third == TileId::Paddle) { paddle_pos = {x, y}; }
                else if(third == TileId::Ball) { ball_pos = {x, y}; }

                replace({x, y}, grid, third);

            } else if(opc == InterruptReason::Input) {
                if(ball_pos.x == paddle_pos.x) cpu.in_.push_back(0);
                else if(ball_pos.x > paddle_pos.x) cpu.in_.push_back(1);
                else if(ball_pos.x < paddle_pos.x) cpu.in_.push_back(-1);
            } 

            if constexpr(SHOULD_PRINT) {
                if(paddle_pos != vec2::null() && ball_pos != vec2::null()) {
                    print(grid, true, false, std::cerr);
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }

            }
        }
        return std::to_string(score);
    }
};

template<>
void solve<Day::day13>(const std::string& ans1, const std::string& ans2) {
    Day13 d(Day::day13, false, ans1, ans2);
    d.solve();
}
