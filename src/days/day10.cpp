#include <day.hpp>

#include <cassert>
#include <vector>
#include <algorithm>
#include <limits>
#include <cmath>
#include <numeric>
#include <set>
#include <iomanip>
#include <thread>

#include "grid.hpp"

struct Day10 : public Solution {
    Grid grid_{{}};
    using Solution::Solution;


    inline bool eq(float one, float two) {
        if(std::isfinite(one) && std::isfinite(two)) {
            return std::fabs(one-two) < std::numeric_limits<float>::epsilon();
        }
        return (std::isnan(one) && std::isnan(two)) || (std::isinf(one) && std::isinf(two));
    }

    inline vec2 get_k(vec2 one, vec2 two) {
        auto dx = two.x-one.x;
        auto dy = two.y-one.y;
        auto g = std::gcd(dx, dy);
        return {dx/g, dy/g};
    }

    vec2 hit_ray(vec2 from, vec2 to, const Grid &grid) {
        if (from == to) return vec2::null();
        const auto k = get_k(from, to);

        auto diff_x = to.x - from.x;
        auto diff_y = to.y - from.y;
        if (std::abs(diff_x) > std::abs(diff_y)) {
            auto t = diff_y / float(diff_x);
            auto sign = (diff_x < 0) ? -1 : 1;
            for (auto i = 0; i <= std::abs(diff_x); ++i) {
                auto p = i * sign;
                vec2 cur(from.x + p, std::round(from.y + p * t));
                if (grid[cur.y][cur.x] == '#' && cur != from && cur != to && get_k(from, cur)==k) return cur;
            }
        }
        else
        {
            auto t = diff_x / float(diff_y);
            auto sign = (diff_y < 0) ? -1 : 1;
            for (auto i = 0; i <= std::abs(diff_y); ++i) {
                auto p = i * sign;
                vec2 cur(std::round(from.x + p * t), from.y + p);
                if (grid[cur.y][cur.x] == '#' && cur != from && cur != to && get_k(from, cur)==k) return cur;
            }
        }
        return vec2::null();
    }

    std::set<vec2> asteroid_list;
    void prepare(std::istream&& is) override {
        std::string line;
        int row{};
        int col{};
        while(std::getline(is, line)) {
            col = 0;
            for(auto c : line) {
                grid_.back().push_back(c);
                if(c == '#') asteroid_list.insert({col, row});
                ++col;
            }
            ++row;
            grid_.push_back({});
        }
        grid_.pop_back();
    }
    vec2 station;

    std::string part1() override { 
        auto max = 0;
        vec2 max_vec;
        for(const auto& a : asteroid_list) {
            auto cur = 0;
            for(const auto& b : asteroid_list) {
                if(a==b) continue;
                if(hit_ray(a, b, grid_) == vec2::null()) {
                    ++cur;
                }
            }
            if(cur > max) {
                max = cur;
                max_vec = a;
            }
        }
        station = max_vec;
        grid_[station.y][station.x] = '*';
        return std::to_string(max);
    }

    static constexpr bool should_print = true;
    std::string part2() override { 
        int num = 0;
        asteroid_list.erase(station);
        constexpr static auto pi = 3.14159265359f;
        auto to_deg = [](auto i) {
            return std::fmod(i*180/pi,360.0f);
        };

        auto get = [=](const vec2& one, const vec2& two) {
            float p = +90+360;
            float first = to_deg(std::atan2(one.y-station.y, one.x-station.x)) + p;
            float second = to_deg(std::atan2(two.y-station.y, two.x-station.x)) + p;
            first = std::fmod(first, 360.0f);
            second = std::fmod(second, 360.0f);

            return std::pair<float, float>(first, second);
        };

        int idx = 0;
        vec2 found;
        while(!asteroid_list.empty()) {
            std::vector<vec2> list (asteroid_list.begin(),asteroid_list.end());
            list.erase(std::remove_if(list.begin(), list.end(), [=](const auto& v){
                return grid_[v.y][v.x]!='#'||hit_ray(station, v, grid_) != vec2::null();
            }), list.end());

            std::sort(list.begin(), list.end(), [=](const vec2& one, const vec2& two){
                auto [first, second] = get(one, two);
                double dist1 = dist(station, one);
                double dist2 = dist(station, two);
                if(eq(first, second)) return  dist1<dist2;
                return first < second;
            });
            for(auto l : list) {
                ++idx;
                if(idx == 200) {
                    found = l;
                    break;
                }
                grid_[l.y][l.x] = '.';
                asteroid_list.erase(l);
                // if(!should_print) {
                //     ray(station, l, grid_);
                // }
            }
        }

        return std::to_string(found.x*100 + found.y);
    }
};

template<>
void solve<Day::day10>(const std::string& ans1, const std::string& ans2) {
    Day10 d(Day::day10, false, ans1, ans2);
    d.solve();
}
