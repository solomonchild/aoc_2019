#include <day.hpp>

#include <algorithm>
#include <iostream>
#include <cassert>
#include <string_utils.hpp>

using std::cout;

//TODO: make it compile on i386!
struct Day22 : public Solution {
    using Solution::Solution;

    using int_type = __int128_t;
    enum class Type {
        DealWithInc,
        NewStack,
        Cut
    };
    struct Order {
        Type type;
        int64_t q{};
    };

    using Orders = std::vector<Order>;
    Orders orders;
    void prepare(std::istream&& is) override {
        std::string line;
        while(std::getline(is, line)) {
            auto parts = split<std::string>(line, " ");
            if(parts[0] == "cut")  orders.push_back({Type::Cut, std::stoi(parts[1])});
            else if(parts[1] == "into") orders.push_back({Type::NewStack});
            else orders.push_back({Type::DealWithInc, std::stoi(*parts.rbegin())});
        }
    }

    struct vec2 {
        int_type a{};
        int_type b{};
    };

    auto mod(int_type a, int_type b) {
        return a%b;
    }

    void cut(vec2& f, int_type n) {
        f.b -= n;
    }

    void deal_with_n(vec2& f, int_type step, int64_t deck_size) {
        f.a = mod(f.a*step, deck_size);
        f.b = mod(f.b*step, deck_size);
    }

    void new_stack(vec2& f, int_type N) {
        f.a *= -1; 
        f.b *= -1;
        f.b += N-1;
    }

    int64_t apply(const vec2& f, int_type idx, int_type deck_size) {
        return (f.a*idx + f.b )%deck_size;
    }

    int_type modInverse(int_type a, int_type m) { 
        int_type x, y; 
        int_type g = gcdExtended(a, m, &x, &y); 
        assert(g == 1);
        int_type res = (x%m + m) % m; 
        return res;
    } 
    int_type fast_pow(int_type x, int_type n, int_type mod) {
        int_type y = 1;
        while(n > 1) {
            if(n&0x1) {
                y*=x;
                y%=mod;
                n-=1;
            } 
            x*=x;
            x%=mod;
            n/=2;
        }
        return ((y*x)%mod);
    }
    
    int_type gcdExtended(int_type a, int_type b, int_type *x, int_type *y) 
    { 
        if (a == 0) 
        { 
            *x = 0, *y = 1; 
            return b; 
        } 
    
        int_type x1, y1; 
        int_type gcd = gcdExtended(b%a, a, &x1, &y1); 
    
        *x = y1 - (b/a) * x1; 
        *y = x1; 
    
        return gcd; 
    } 

    std::string part1() override {
        constexpr const static size_t deck_size = 10'007;
        vec2 f{1,0};

        for(auto order : orders) {
            if(order.type == Type::Cut) {
                cut(f, order.q);
            } else if(order.type == Type::DealWithInc) {
                deal_with_n(f, order.q, deck_size);
            } else if(order.type == Type::NewStack) {
                new_stack(f, deck_size);
            }
        }
        assert(apply(f, 2019, deck_size) == 2558);
        return std::to_string(apply(f, 2019, deck_size));
    }

    std::string part2() override { 
        constexpr int64_t deck_size = 119315717514047;
        constexpr int64_t iterations = 101741582076661;
        auto reversed = orders;
        std::reverse(reversed.begin(), reversed.end());
        vec2 f {1, 0};

        for(auto order : reversed) {
            if(order.type == Type::Cut) {
                cut(f, -order.q);
            } else if(order.type == Type::DealWithInc) {
                deal_with_n(f, modInverse(order.q, deck_size), deck_size);
            } else if(order.type == Type::NewStack) {
                new_stack(f, deck_size);
            }
        }
        //put those coefficients into some math packet
        // res = a^n*x + b*(1-a^n)/(1-a)
        // for "/(1-a)" you can calculate modular inverse and multiply with that
        // for "a^n" you can calculate power mod
        assert(f.a == 90816468577800 && f.b == 86595210541440);
        auto an = fast_pow(f.a, iterations, deck_size);
        auto inv = modInverse(1-f.a, deck_size);
        auto ans = an*2020 + (f.b*(1-an)%deck_size)*inv%deck_size;
        return std::to_string(int64_t(ans%deck_size));

        return "63967243502561";
    }
};

template<>
void solve<Day::day22>(const std::string& ans1, const std::string& ans2) {
    Day22 d(Day::day22, false, ans1, ans2);
    d.solve();
}
