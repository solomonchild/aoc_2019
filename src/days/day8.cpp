#include <day.hpp>

#include <cassert>
#include <vector>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <limits>

struct Day8 : public Solution {
    using Solution::Solution;

    std::vector<std::vector<int>> layers{{}};

    int layer_with_least0 = 0;

    void prepare(std::istream&& is) override {
        auto min_zeros = std::numeric_limits<int>::max();

        while(is) {
            auto zeros = 0;
            std::istreambuf_iterator<char> it(is);
            std::istreambuf_iterator<char> end{};
            if(it == end) break;
            while(it != end && layers.back().size() < 25*6) {
                auto num = (*it++) - '0'; 
                if(!num) ++zeros;
                layers.back().push_back(num);
            }
            if(zeros < min_zeros) {
                min_zeros = zeros;
                layer_with_least0 = layers.size()-1;
            }
            layers.push_back({});
        }
        if(layers.back().empty()) layers.pop_back();
    }

    std::string part1() override { 
        return std::to_string(std::count(layers[layer_with_least0].begin(), layers[layer_with_least0].end(), 1) 
        * std::count(layers[layer_with_least0].begin(), layers[layer_with_least0].end(), 2));
    }
    std::string part2() override { 
        std::stringstream ss("\n");
        ss.seekp(0, std::ios_base::end);

        for(auto row = 0; row < 6; ++row) {
            for (auto col = 0; col < 25; ++col) {
                for(auto& layer : layers) {
                    if(auto p = layer[row*25+col]; p == 2) continue;
                    else {
                        ss << ((p==0)?" ":"*");
                        break;
                    }
                }
            }
            ss << "\n";
        }

        return ss.str();
    }
};

template<>
void solve<Day::day8>(const std::string& ans1, const std::string& ans2) {
    Day8 d(Day::day8, false, ans1, ans2);
    d.solve();
}
