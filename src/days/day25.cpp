#include <day.hpp>
#include <iostream>
#include <string>

#include <int_code.hpp>
#include <grid.hpp>

using std::cout;

struct Day25 : public Solution {
    using Solution::Solution;

    Program master_program;
    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    void push(CPU& cpu, const std::string& cmd) {
        for(auto c : cmd) {
            cpu.in_.push_back(c);
        }
        cpu.in_.push_back('\xA');
    }
    std::string get_output(CPU& cpu, Program& p) {
        std::string res;
        for(auto reason = cpu.run(p); reason == InterruptReason::Output; reason = cpu.run(p)) {
            res += cpu.out_;
        }
        return res;
    }


    std::string part1() override { 
        CPU robot;
        auto grid = cons_grid({100, 100}, ' ');
        auto local = master_program;
        auto reason = robot.run(local);

        std::string next_cmd;

        while(reason != InterruptReason::Halt) {
            if(reason == InterruptReason::Output) {
                auto out = get_output(robot, local);
                std::cout << out;
            }
            else if(reason == InterruptReason::Input) {
                std::string cmd;
                std::getline(std::cin, cmd);
                next_cmd = cmd;
            }

            if(!next_cmd.empty()) {
                push(robot, next_cmd);
                next_cmd.clear();
            }
            reason = robot.run(local);
        }
        return Solution::part1();
    }

    std::string part2() override { 
        return Solution::part2();
    }
};

template<>
void solve<Day::day25>(const std::string& ans1, const std::string& ans2) {
    Day25 d(Day::day25, false, ans1, ans2);
    d.solve();
}
