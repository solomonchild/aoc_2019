#include <day.hpp>

#include <iostream>
#include <string>

using std::cout;

struct Day4 : public Solution {
    using Solution::Solution;
    int from{};
    int to{};
    void prepare(std::istream&& is) override {
        char throwaway;
        is >> from >> throwaway >> to;
    }

    bool is_valid(int num, bool part1 = true) {
        auto str = std::to_string(num);
        bool is_double{};

        if(str.size() != 6) return false;

        for(auto i = 0; i < str.size()-1; ++i) {
            if(str[i] > str[i+1]) return false;
            if(!is_double && str[i] == str[i+1]) {
                if(part1) is_double = true;
                else {
                    auto how_many_doubles = 2;
                    auto j = i+2;
                    while(j < str.size() && str[i] == str[j]) {
                        ++how_many_doubles;
                        ++j;
                    }
                    i = j-2;
                    is_double = (how_many_doubles == 2);
                }
            }
        }
        return is_double;
    }

    std::string part1() override { 
        auto res = 0;
        for(auto i = from; i <= to; ++i) {
            if(is_valid(i)) ++res;
        }
        return std::to_string(res);
    }
    std::string part2() override { 
        auto res = 0;
        for(auto i = from; i <= to; ++i) {
            if(is_valid(i, false)) ++res;
        }
        return std::to_string(res);
    }
};

template<>
void solve<Day::day4>(const std::string& ans1, const std::string& ans2) {
    Day4 d(Day::day4, false, ans1, ans2);
    d.solve();
}
