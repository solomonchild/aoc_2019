#include <day.hpp>

#include <algorithm>
#include <array>
#include <iterator>
#include <string>
#include <vector>

#include "string_utils.hpp"
#include "int_code.hpp"

struct Day7 : public Solution {
    using Solution::Solution;

    Program master_program;

    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    std::string part1() override { 
        int max = 0;
        std::array<int, 5> max_signal;
        std::array<CPU,5> cpus;
        std::array<int,5> signal = {0,1,2,3,4};

        do {
            int output{};
            for(auto amp = 0; amp < 5; ++amp) {
                cpus[amp].reset();
                cpus[amp].in_ = {
                    signal[amp],
                    output
                };

                auto local_pr = master_program;
                cpus[amp].run(local_pr);
                output = cpus[amp].out_;
            }

            if(output > max) {
                max = output;
                max_signal = signal;
            }
        } while(std::next_permutation(signal.begin(), signal.end()));
        return std::to_string(max);
    }

    std::string part2() override { 
        std::vector<int> signal = {5,6,7,8,9};
        int max = 0;
        std::vector<int> max_signal;
        std::vector<CPU> amps(5);
        std::vector<Program> programs(5, master_program);

        do {
            bool stop = false;
            for(auto i = 0; i < amps.size(); ++i) {
                amps[i].reset();
                amps[i].in_.push_back(signal[i]);
            }

            int output = 0;
            while(!stop) {
                for(auto i = 0; i < amps.size(); ++i) {
                    auto& amp = amps[i];
                    amp.in_.push_back(output);

                    auto reason = amp.run(programs[i]);
                    if(reason == InterruptReason::Halt ) {
                        stop = true;
                        break;
                    } else if(reason == InterruptReason::Output) {
                        output = amp.out_;
                    } 
                }
            }
            if(amps.back().out_> max) {
                max = amps.back().out_;
                max_signal = signal;
            }
        } while(std::next_permutation(signal.begin(), signal.end()));
        return std::to_string(max);
    }
};

template<>
void solve<Day::day7>(const std::string& ans1, const std::string& ans2) {
    Day7 d(Day::day7, false, ans1, ans2);
    d.solve();
}
