#include <day.hpp>

#include <string>
#include <cassert>
#include <iostream>

#include <grid.hpp>
using std::cout;

struct Day24 : public Solution {
    using Solution::Solution;

    const vec2 max{5,5};

    Grid master_grid = cons_grid(max, '.');

    void prepare(std::istream&& is) override {
        std::string line;
        auto idx = 0;
        while(std::getline(is, line)) {
            std::copy(line.begin(), line.end(), master_grid[idx].begin());
            ++idx;
        }
    }

    std::vector<vec2> neighbors(vec2 node) {
        std::vector<vec2> vecs;
        if(node.x > 0) vecs.emplace_back(node.x-1, node.y);
        if(node.x < max.x-1) vecs.emplace_back(node.x+1, node.y);
        if(node.y > 0) vecs.emplace_back(node.x, node.y-1);
        if(node.y < max.y - 1) vecs.emplace_back(node.x, node.y+1);
        return vecs;
    }

    auto& at(vec2 v, Grid& grid) {
        return grid[v.y][v.x];
    }

    auto at(vec2 v, const Grid& grid) {
        return grid[v.y][v.x];
    }

    int bugs(const std::vector<vec2>& vecs, const Grid& grid) {
        return std::count_if(vecs.begin(), vecs.end(), [&](const auto& v){
            return at(v, grid) == '#';
        });
    }
    
    auto get_new_cur(char cur, int number_of_bugs) {
        if(cur == '#' && number_of_bugs != 1) return '.';
        else if(cur == '.' && (number_of_bugs == 1 || number_of_bugs == 2)) return  '#';
        return cur;
    }

    std::string part1() override { 
        std::set<Grid> seen_grids;
        auto local_grid = master_grid;
        for(auto min = 0; min < 400; ++min) {
            auto new_grid = local_grid;
            for(auto row = 0; row < max.y; ++row) {
                for(auto col = 0; col < max.x; ++col) {
                    auto ns = neighbors({col, row});
                    at({col ,row}, new_grid) = get_new_cur(at({col ,row}, local_grid), bugs(ns, local_grid));
                }
            }
            std::swap(local_grid, new_grid);
            if(!seen_grids.insert(local_grid).second) {
                break;
            }
        }

        long long total{};
        for(auto i = 0; i < max.y; ++i) {
            for(auto j = 0; j < max.x; ++j) {
                if(at({j,i}, local_grid) == '#') {
                    total += std::pow(2, i*max.x + j);
                }
            }
        }
        return std::to_string(total);
    }

    using Maze = std::map<int, Grid>; 
    struct vec3 {
        int x{}, y{}, level{};
        vec3(int x, int y, int level) 
        :x(x), y(y), level(level) {}
        bool operator<(const vec3& other) const {
            if(x == other.x) {
                if(y == other.y) return level < other.level;
                return y < other.y;
            }
            return x < other.x;
        }
    };

    auto& at(vec3 v, Maze& m) {
        return m[v.level][v.y][v.x];
    }

    auto at(vec3 v, const Maze& m) {
        return m.at(v.level)[v.y][v.x];
    }

    int min_level = 0;
    int max_level = 0;
    auto neighbours(vec3 node, const Maze& m) {
        assert(node.level >= min_level && node.level <= max_level);
        std::vector<vec3> vecs;
        if(node.x > 0 /*&& vec2{node.x-1, node.y} != vec2{2,2}*/) vecs.emplace_back(node.x-1, node.y, node.level);
        if(node.x < max.x-1 /*&& vec2{node.x+1, node.y} != vec2{2,2}*/ ) vecs.emplace_back(node.x+1, node.y, node.level);
        if(node.y > 0 /*&& vec2{node.x, node.y-1} != vec2{2,2}*/) vecs.emplace_back(node.x, node.y-1, node.level);
        if(node.y < max.y-1 /*&& vec2{node.x, node.y+1} != vec2{2,2}*/) vecs.emplace_back(node.x, node.y+1, node.level);

        // center: {2,2}
        //  01234
        //0 ooooo
        //1 o.x.o
        //2 ox?xo
        //3 o.x.o
        //4 ooooo

        int plus_level = node.level+1;
        if(plus_level <= max_level) {
            if     (node.x == 2 && node.y == 1) for(auto i = 0; i < 5; ++i) vecs.emplace_back(i,0, plus_level);
            else if(node.x == 2 && node.y == 3) for(auto i = 0; i < 5; ++i) vecs.emplace_back(i,4, plus_level);
            else if(node.x == 1 && node.y == 2) for(auto i = 0; i < 5; ++i) vecs.emplace_back(0, i, plus_level);
            else if(node.x == 3 && node.y == 2) for(auto i = 0; i < 5; ++i) vecs.emplace_back(4, i, plus_level);
        }

        int minus_level = node.level-1;
        if(minus_level >= min_level) {
            if(node.y == 0) vecs.emplace_back(2, 1, minus_level);
            if(node.y == 4) vecs.emplace_back(2, 3, minus_level);

            if(node.x == 0) vecs.emplace_back(1,2, minus_level);
            if(node.x == 4) vecs.emplace_back(3,2, minus_level);
        }
        return vecs;
    }

    int bugs(const std::vector<vec3>& vecs, const Maze& maze) {
        return std::count_if(vecs.begin(), vecs.end(), [&](const auto& v){
            return at(v, maze) == '#';
        });
    }

    void print_maze(const Maze& m) {
        for(auto level = min_level; level <= max_level; ++level) {
            cout << "Level: " << level << "\n";
            assert(m.find(level) != m.end());
            print(m.at(level));
        }
    }

    void new_level (Maze& m, int level) {
        if(m.find(level) == m.end()) {
            m[level] = cons_grid(max, '.');
            m[level][2][2] = '?';
        }
    }

    int bugs(Maze& m) {
        int total = 0;
        for(auto level = min_level; level <= max_level; ++level) {
            for(auto row = 0; row < max.y; ++row) {
                for(auto col = 0; col < max.x; ++col) {
                    if(at({col,row,level}, m) == '#') ++total;
                }
            }
        }
        return total;
    }

    std::string part2() override { 
        Maze maze;
        maze[0] = master_grid;
        constexpr auto minutes = 200;
        for(auto i = 0; i < minutes; ++i) {
            --min_level;
            ++max_level;
            new_level(maze, min_level);
            new_level(maze, max_level);
            auto new_maze = maze;

            for(auto level = min_level; level <= max_level; ++level) {
                for(auto row = 0; row < max.y; ++row) {
                    for(auto col = 0; col < max.x; ++col) {
                        if(vec2{col,row} == vec2{2,2}) continue;
                        auto ns = neighbours({col, row, level}, maze);
                        at({col,row,level}, new_maze) = get_new_cur(at({col,row,level},maze), bugs(ns, maze));
                    }
                }
            }
            std::swap(new_maze, maze);
        }
        return std::to_string(bugs(maze));
    }
};

template<>
void solve<Day::day24>(const std::string& ans1, const std::string& ans2) {
    Day24 d(Day::day24, false, ans1, ans2);
    d.solve();
}
