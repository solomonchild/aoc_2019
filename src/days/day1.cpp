#include <day.hpp>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

#include <range/v3/all.hpp>

struct Day1 : public Solution {
    using Solution::Solution;

    long long sum{};
    long long total{};

    std::string part1(std::istream& is) override { 
        ranges::istream_view<int> it(is);
        return std::to_string(ranges::accumulate(it | ranges::views::transform([](int i){ return i/3-2;}), 0));
    }

    std::string part2(std::istream& is) override { 
        ranges::istream_view<int> it(is);
        return std::to_string(ranges::accumulate(it | ranges::views::transform([](int i){ 
            auto cur = i/3-2;
            int res{};
            while(cur > 0) { 
                res += cur;
                cur = cur/3-2;
            }
            return res;
        }), 0));
    }
};

template<>
void solve<Day::day1>(const std::string& ans1, const std::string& ans2) {
    Day1 d(Day::day1, true, ans1, ans2);
    d.solve();
}
