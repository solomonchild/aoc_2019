#include <day.hpp>

#include <algorithm>
#include <cassert>
#include <unordered_map>
#include <vector>
#include <string>

#include <string_utils.hpp>

static inline long long int_div(long long first, long long second) {
    return (first+second-1)/second;
}

struct Component {
    std::string what;
    int how_much_needed{};
};

using Components = std::vector<Component>;
struct Entry {
    int produce_num{};
    Components components;
};

struct Day14 : public Solution {
    using Solution::Solution;

    std::unordered_map<std::string, Entry> recipes;

    void prepare(std::istream&& is) override {
        std::string line;
        while(std::getline(is, line)) {
            auto lhs_rhs = split<std::string>(line, "=>");
            auto lhs = split<std::string>(lhs_rhs[0], ",");
            auto rhs = strip(lhs_rhs[1]);
            auto rhs_split = split<std::string>(rhs, " ");
            for(auto l : lhs ) {
                l = strip(l);
                recipes[rhs_split[1]].produce_num = std::stoi(rhs_split[0]);
                recipes[rhs_split[1]].components.push_back({split<std::string>(l," ")[1], std::stoi(split<std::string>(l," ")[0])});
            }
        }
    }

    long long expand(const std::string& what, int N) {
        std::unordered_map<std::string, long long> req{{what, N}};
        std::unordered_map<std::string, long long> surplus;

        int c{};
        do {
            c = 0;
            auto temp = req;
            for(auto [elem_name, how_much_req] : req) {
                if(auto it = recipes.find(elem_name); it != recipes.end()) {
                    assert(!surplus[elem_name]);

                    ++c;

                    how_much_req = std::max(how_much_req, temp[elem_name]);
                    temp.erase(elem_name);

                    const auto produce_num = it->second.produce_num;
                    const auto apply_nums = int_div(how_much_req, produce_num);

                    surplus[elem_name] += apply_nums*produce_num - how_much_req;
                    
                    for(const auto& r : it->second.components) {
                        auto total = apply_nums*r.how_much_needed;
                        assert(total >= 0);

                        auto diff = std::min(total, surplus[r.what]);
                        assert(diff >= 0);

                        total -= diff;
                        surplus[r.what] -= diff;

                        if(total) temp[r.what] += total;
                    }
                }
            }
            std::swap(req, temp);
        } while(c);
        return req["ORE"];
    }

    std::string part1() override { 
        auto parts = expand("FUEL", 1);
        return std::to_string(parts);
    }
    std::string part2() override { 
        long long max_ore = 1'000'000'000'000LL;
        int lo = 0;
        int hi = 10'000'000;
        while(lo < hi) {
            int mid = (lo+hi)/2;
            if(expand("FUEL", mid) > max_ore) hi = mid-1;
            else lo = mid+1;
        }
        if(expand("FUEL", lo) > max_ore) --lo;
        return std::to_string(lo);
    }
};

template<>
void solve<Day::day14>(const std::string& ans1, const std::string& ans2) {
    Day14 d(Day::day14, false, ans1, ans2);
    d.solve();
}
