#include <day.hpp>

#include <algorithm>
#include <numeric>
#include <valarray>
#include <iterator>
#include <vector>
#include <iostream>
#include <sstream>

using std::cout;

struct Day16 : public Solution {
    using Solution::Solution;

    std::vector<int> signal;

    void prepare(std::istream&& is) override {
        std::string line;
        std::getline(is, line);
        std::transform(line.begin(), line.end(), std::back_inserter(signal), [](const auto c){ return c - '0'; });
    }

    using Matrix = std::vector<std::valarray<int>>;

    Matrix get_matrix(size_t size) {
        Matrix matrix(size);
        static const auto pattern = std::vector<int>{0, 1, 0, -1};

        for(auto row = 0; row < size; ++row) {
            matrix[row].resize(size);
            auto pat_idx = 0;
            const auto repeat = row+1;

            for(auto i = 0; i < row; ++i) {
                matrix[row][i] = pattern[0];
            }

            for(auto i = row; i < size; ++i) {
                if(!( (i-row)%repeat )) pat_idx = (pat_idx+1)%pattern.size();
                matrix[row][i] = pattern[pat_idx];
            }
        }
        return matrix;
    }

    std::string part1() override { 
        std::valarray<int> arr(signal.size());
        for(auto i = 0; i < signal.size(); ++i) arr[i] = signal[i];

        auto prev = arr;
        auto matrix = get_matrix(arr.size());

        for(auto phase = 0; phase < 100; ++phase) {
            for(auto i = 0; i < arr.size(); ++i) {
                prev[i] = std::abs((arr*matrix[i]).sum());
                prev %= 10;
            }
            arr = prev;
        }
        std::stringstream ss;
        std::ostream_iterator<int> oi(ss);
        std::copy(begin(arr), std::next(begin(arr),8), oi);

        return ss.str();
    }
    std::string part2() override { 

        size_t idx = std::accumulate(begin(signal), std::next(begin(signal), 7), 0, [](const auto& acc, const auto& b){
            return acc*10 + b;
        });

        std::vector<int> arr = [idx, this]() {
            const auto size = signal.size()*10'000;
            std::vector<int> temp(size-idx);
            for(size_t i = 0; i < temp.size(); ++i) {
                temp[i] = signal[(idx+i)%signal.size()];
            }
            return temp;
        }();

        for(auto phase = 0; phase < 100; ++phase) {
            std::partial_sum(arr.rbegin(), arr.rend(), arr.rbegin(), [](int c1, int c2){
                return (c1+c2)%10;
            });
        }

        std::stringstream ss;
        std::ostream_iterator<int> oi(ss);
        std::copy(begin(arr), std::next(begin(arr),8), oi);

        return ss.str();
    }
};

template<>
void solve<Day::day16>(const std::string& ans1, const std::string& ans2) {
    Day16 d(Day::day16, false, ans1, ans2);
    d.solve();
}
