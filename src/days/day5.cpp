#include <day.hpp>

#include <functional>
#include <unordered_map>

#include "string_utils.hpp"
#include "int_code.hpp"

struct Day5 : public Solution {
    using Solution::Solution;

    Program program;
    void prepare(std::istream&& is) override {
        program = parse(std::move(is));
    }

    std::string part1() override { 
        auto local_program = program;
        CPU cpu;
        cpu.in_.push_back(1);
        while(cpu.run(local_program) != InterruptReason::Halt)
        ;
        return std::to_string(cpu.out_);
    }

    std::string part2() override { 
        auto local_program = program;
        CPU cpu;
        cpu.in_.push_back(5);
        while(cpu.run(local_program) != InterruptReason::Halt)
        ;
        return std::to_string(cpu.out_);
    }
};

template<>
void solve<Day::day5>(const std::string& ans1, const std::string& ans2) {
    Day5 d(Day::day5, false, ans1, ans2);
    d.solve();
}
