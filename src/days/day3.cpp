#include <day.hpp>

#include <algorithm>
#include <cassert>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

struct Day3 : public Solution {
    using Solution::Solution;
    struct Dir {
        enum class Where {
            Up, Right, Down, Left
        };
        Dir(const std::string& str) {
            switch(str[0]) {
                case 'D': where = Where::Down; break;
                case 'U': where = Where::Up; break;
                case 'L': where = Where::Left; break;
                case 'R': where = Where::Right; break;
            }
            how_much = std::atoi(str.substr(1).c_str());
        }

        Where where;
        size_t how_much{};
    };

    static std::vector<Dir> split(const std::string &str, const std::string &delims = ",")
    {
        std::vector<Dir> res;
        size_t idx = 0;
        while (auto end = str.find_first_of(delims, idx))
        {
            auto sub = str.substr(idx, end - idx);

            if (!sub.empty())
            {
                res.emplace_back(str.substr(idx, end - idx));
            }
            idx = end + 1;
            if (end == std::string::npos)
                break;
        }
        return res;
    }

    struct vec2{
        int x{}, y{}; 

        bool operator==(const vec2& other) const {
            return x == other.x && y == other.y;
        }

        bool operator<(const vec2& other) const {
            if(x == other.x) return y < other.y;
            else return x < other.x;
        }
    };


    std::map<vec2, vec2> map;
    std::set<vec2> candidates;

    void trace_directions(const std::vector<Dir>& dirs, std::function<void(const vec2& v, int steps)>&& cbk) {
        int pos_x{}, pos_y{};
        int steps{};

        for(auto where : dirs) {
            auto delta = where.how_much;
            auto sign = (where.where == Dir::Where::Down || where.where == Dir::Where::Left)?-1:1;

            if(where.where == Dir::Where::Left || where.where == Dir::Where::Right) {
                for(auto i = 1; i <= delta; ++i) {
                    auto p = vec2{pos_x + i*sign, pos_y};
                    cbk(p, steps+i);
                }
                pos_x += delta *sign;
                steps += delta;
            }
            else {
                for(auto i = 1; i <= delta; ++i) {
                    auto p = vec2{pos_x, pos_y+i*sign};
                    cbk(p, steps+i);
                }
                pos_y += delta *sign;
                steps += delta;
            }
        }
    }

    void prepare(std::istream&& is) override {
        std::string line;
        std::getline(is, line);

        trace_directions(split(line), [this](const vec2& v, int steps){
            map[v] = {1, steps};
        });

        std::getline(is, line);

        trace_directions(split(line), [this](const vec2 &v, int steps) {
            if (map[v].x == 1) {
                candidates.insert(v);
                map[v] = {2, map[v].y + steps};
            }
        });
    }

    std::string part1() override { 
        std::vector<int> ints;
        std::transform(candidates.begin(), candidates.end(), std::back_inserter(ints), [this](const vec2& v) {
            return std::abs(v.y) + std::abs(v.x);
        });
        auto it = std::min_element(ints.begin(), ints.end());
        assert(it != ints.end());
        return std::to_string(*it);
    }
    std::string part2() override { 
        std::vector<int> ints;
        std::transform(candidates.begin(), candidates.end(), std::back_inserter(ints), [this](const vec2& v) {
            return map[v].y;
        });
        auto it = std::min_element(ints.begin(), ints.end());
        assert(it != ints.end());
        return std::to_string(*it);
    }
};

template<>
void solve<Day::day3>(const std::string& ans1, const std::string& ans2) {
    Day3 d(Day::day3, false, ans1, ans2);
    d.solve();
}
