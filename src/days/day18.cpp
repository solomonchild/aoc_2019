#include <day.hpp>

#include <cassert>
#include <cctype>
#include <deque>
#include <functional>
#include <limits>
#include <map>
#include <set>
#include <unordered_set>

#include <grid.hpp>

using Vectors = std::vector<vec2>;
using KeySet = std::unordered_set<char>;
using std::cout;
const std::set<char> ROBOTS {'@', '$', '&', '*'};

inline bool is_door(char c) {
    return std::isalpha(c) && std::isupper(c);
}

inline bool is_door(vec2 v, const Grid& g) {
    auto cell = g[v.y][v.x];
    return is_door(cell);
}

inline bool is_key(char c) {
    return std::isalpha(c) && std::islower(c);
}

inline bool is_key(vec2 v, const Grid& g) {
    auto cell = g[v.y][v.x];
    return is_key(cell);
}

bool has_keys_or_doors(const Grid& grid) {
    for(auto& r : grid) {
        if(std::any_of(r.begin(), r.end(), [](const auto c) {return std::isalpha(c);})) {
            return true;
        }
    }
    return false;
}



struct Day18 : public Solution {
    using Solution::Solution;

    Grid grid;
    std::map<char, vec2> keys;
    std::map<char, vec2> doors;
    std::set<vec2> robots;
    int MAX_PATH = 0;

    void prepare(std::istream&& is) override {
        std::string line;
        while(std::getline(is, line)) {
            grid.push_back(Row{line.begin(), line.end()});
            for(auto c = 0; c < line.size(); ++c) {
                if(is_key(line[c])) keys[line[c]] = vec2(c, grid.size()-1);
                else if(is_door(line[c])) doors[line[c]] = vec2(c, grid.size()-1);
            }
            if(auto idx = line.find('@'); idx != std::string::npos) {
                robots.insert(vec2(idx, grid.size()-1));
            }
        }
        MAX_PATH=keys.size();
    }

    Vectors neighbors(vec2 node) {
        Vectors vecs;
        if(node.x > 0 && grid[node.y][node.x-1]!='#') vecs.emplace_back(node.x-1, node.y);
        if(!grid.empty() && node.x < grid[0].size()-1 && grid[node.y][node.x+1] != '#') vecs.emplace_back(node.x+1, node.y);
        if(node.y > 0 && grid[node.y-1][node.x] != '#') vecs.emplace_back(node.x, node.y-1);
        if(node.y < grid.size()-1 && grid[node.y+1][node.x] != '#') vecs.emplace_back(node.x, node.y+1);
        return vecs;
    }

    struct info {
        int dist{};
        std::string doors;
        std::string keys;
    };
    using Dists = std::map<char, std::map<char, info>>;

    char& at(const vec2& v) {
        return grid[v.y][v.x];
    }
    Dists dists;

    void BFS(vec2 start) {
        std::deque<std::tuple<vec2, info>> queue{{start, {0, {}, {}}}};
        std::set<vec2> seen;
        int total_steps = 0;
        int step = 1;

        while(!queue.empty()) {
            while(step) {
                auto [node, inf] = queue.front(); queue.pop_front();
                --step;
                if(!seen.insert(node).second) continue;
                auto [distance,doors,keys] = inf;

                if(auto c = at(node); std::isalpha(c) && node != start) {
                    if(is_door(c)) doors += c;
                    if(is_key(c)) {
                        dists[at(start)][c].doors = doors;
                        dists[at(start)][c].keys = keys;
                        dists[at(start)][c].dist = distance;
                        keys += c;
                    }
                }

                for(auto neighbor : neighbors(node)) {
                    queue.emplace_back(neighbor, info{total_steps+1, doors, keys});
                }
            }
            step = queue.size();
            ++total_steps;
        }
    }
    std::unordered_map<std::string, int> cache;

    using KeyDists = std::vector<std::pair<std::string, int>>;
    auto viable_keys(std::string keys, std::string have_keys, bool clear = false) {
        static std::unordered_map<std::string, std::unordered_map<std::string, KeyDists>> cache;
        if(clear) {
            cache.clear();
        }

        std::sort(keys.begin(), keys.end());
        std::sort(have_keys.begin(), have_keys.end());

        if(auto it = cache[keys].find(have_keys); it != cache[keys].end()) {
            return it->second;
        }

        KeyDists ret;
        for(auto key_idx = 0; key_idx < 4; ++key_idx) {
            auto key = keys[key_idx];
            auto new_keys = keys;

            for(const auto& [to, inf] : dists[key]) {
                new_keys[key_idx] = to;
                if(have_keys.find(to) != std::string::npos) continue;

                if(std::all_of(inf.doors.begin(), inf.doors.end(),
                               [&](auto c) { return have_keys.find(std::tolower(c)) != std::string::npos; }) &&
                   std::all_of(inf.keys.begin(), inf.keys.end(),
                               [&](auto c) { return have_keys.find(std::tolower(c)) != std::string::npos; })) {

                    ret.push_back({new_keys, inf.dist});
                }
            }
        }
        cache[keys][have_keys] = ret;
        return ret;
    }

    int Dijkstra() {

        struct KeySetDist {
            std::string keyset;
            int dist{};
        };

        for(auto [k,pos] : keys) {
            BFS(pos);
        }
        for(auto r : robots)
            BFS(r);

        std::unordered_map<
            std::string, 
            std::unordered_map<std::string, int>
        > distmap;

        std::priority_queue<
            std::pair<std::string, std::string>, 
            std::vector<std::pair<std::string, std::string>>,
            std::function<bool(const std::pair<std::string, std::string>&, const std::pair<std::string, std::string>&)>>
            queue([&](const auto& p1, const auto& p2) {
                if(p1.second.size() == p2.second.size()) { return distmap[p1.first][p1.second] < distmap[p2.first][p2.second]; }
                return p1.second.size() < p2.second.size();
            });

        assert(robots.size() <= 4);
        distmap["@$*&"][""] = 0;
        if(robots.size() == 4) queue.push({"@$*&", "" });
        else queue.push({"@", "" });

        std::string min_keys = "";
        viable_keys("", "", true);
        while(!queue.empty()) {
            auto [keys, keys_got] = queue.top(); queue.pop();

            auto sorted_keys = keys_got;
            std::sort(sorted_keys.begin(), sorted_keys.end());
            auto dist_to_cur_key = distmap[keys][sorted_keys];

            for(auto k : keys) {
                if(!ROBOTS.count(k) && keys_got.find(k) == std::string::npos) sorted_keys += k;
            }

            std::sort(sorted_keys.begin(), sorted_keys.end());

            if(keys_got.size() == MAX_PATH - 1) {
                distmap[keys][sorted_keys] = dist_to_cur_key;
            }

            for(auto [next, dist_to_next] : viable_keys(keys, sorted_keys)) {

                if(auto it = distmap[next].find(sorted_keys);
                   it == distmap[next].end() || it->second > dist_to_cur_key + dist_to_next) {

                    distmap[next][sorted_keys] = dist_to_cur_key + dist_to_next;
                    queue.push({next, sorted_keys});
                }
            }
        }
        int min = std::numeric_limits<int>::max();
        for(auto [key, keys_dist] : distmap) {
            for(auto [keys, dist] : keys_dist) {
                if(keys.size() == MAX_PATH) {
                    if(dist < min) {
                        min = dist;
                        min_keys = keys;
                    }
                }
            }
        }
        return min;
    }

    std::string part1() override { 
        return std::to_string(Dijkstra());
    }

    std::string part2() override { 
        dists.clear();
        auto start = *robots.begin();
        grid[start.y][start.x] = '#';
        grid[start.y][start.x+1] = '#';
        grid[start.y][start.x-1] = '#';
        grid[start.y+1][start.x] = '#';
        grid[start.y-1][start.x] = '#';

        robots.clear();

        grid[start.y+1][start.x+1] = '@';
        robots.insert({start.x+1, start.y+1});

        grid[start.y-1][start.x-1] = '$';
        robots.insert({start.x-1, start.y-1});

        grid[start.y+1][start.x-1] = '*';
        robots.insert({start.x-1, start.y+1});

        grid[start.y-1][start.x+1] = '&';
        robots.insert({start.x+1, start.y-1});
        return std::to_string(Dijkstra());
    }
};

template<>
void solve<Day::day18>(const std::string& ans1, const std::string& ans2) {
    Day18 d(Day::day18, false, ans1, ans2);
    d.solve();
}
