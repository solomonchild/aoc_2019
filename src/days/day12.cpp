#include <day.hpp>

#include <cassert>
#include <iostream>
#include <numeric>
#include <set>
#include <string>

#include "string_utils.hpp"
#include "vec3.hpp"

//TODO: particle?
struct Moon {
    vec3 pos;
    vec3 vel;

    long long pot() const {
        return std::abs(pos.x) + std::abs(pos.y) + std::abs(pos.z);
    }

    long long kin() const {
        return std::abs(vel.x) + std::abs(vel.y) + std::abs(vel.z);
    }

    long long total() const {
        return kin() * pot();
    }

    bool operator==(const Moon& m) const {
        return pos == m.pos && vel == m.vel;
    }

    bool operator<(const Moon& v) const {
        if(vel == v.vel) return pos < v.pos;
        else return vel < v.vel;
    }
    friend std::ostream& operator<<(std::ostream& os, const Moon& m) {
        os << "pos: " << m.pos << ", vel: " << m.vel;
        return os;
    }
};

struct Day12 : public Solution {
    using Solution::Solution;

    using Moons = std::vector<Moon>;
    Moons moons;

    void prepare(std::istream&& is) override {
        std::string line;
        while(std::getline(is, line)) {

            line = line.substr(1);
            line.erase(line.end()-1);

            auto constituents = split<std::string>(line, ", ");
            auto x_expr = split<std::string>(constituents[0],"=");
            auto y_expr = split<std::string>(constituents[1],"=");
            auto z_expr = split<std::string>(constituents[2],"=");

            moons.push_back({});
            moons.back().pos = vec3{std::stoi(x_expr[1]), std::stoi(y_expr[1]), std::stoi(z_expr[1])};
        }
    }

    void update(Moons& moons, bool only_one = false, int axis = -1) {
        const auto start = only_one?axis:0;
        const auto end = only_one?axis+1:3;
        assert(start >= 0 && end <= 3);

        for (auto i = 0; i < moons.size()-1; ++i ) {
            auto& m = moons[i];

            for (auto j = i+1; j < moons.size(); ++j) {

                auto& n = moons[j];
                for (auto i = start; i < end; ++i) {
                    auto p = 1;

                    if (n.pos[i] > m.pos[i]) {
                        p *= -1;
                    }
                    else if (n.pos[i] == m.pos[i]) p = 0; 

                    n.vel[i] += p;
                    m.vel[i] -= p;
                }

            }
        }

        for(auto &m : moons) {
            for(auto i = start; i < end; ++i) {
                m.pos[i] += m.vel[i];
            }
        }
    }

    std::string part1() override { 
        auto ms = moons;
        for(auto step = 0; step < 1000; ++step) {
            update(ms);
        }

        long long total{};
        for(auto& m : ms) {
            total += m.total();
        }

        return std::to_string(total);
    }

    std::string part2() override { 
        vec3 idx;
        auto ms = moons;
        std::set<Moons> moon_set;
        while(moon_set.insert(ms).second) {
            update(ms, true, 0);
            ++idx.x;
        }

        ms = moons;
        moon_set.clear();
        while(moon_set.insert(ms).second) {
            update(ms, true, 1);
            ++idx.y;
        }

        ms = std::move(moons);
        moon_set.clear();
        while(moon_set.insert(ms).second) {
            update(ms, true, 2);
            ++idx.z;
        }

        return std::to_string(std::lcm<long long>(std::lcm<long long>(idx.x, idx.y), idx.z));
    }
};

template<>
void solve<Day::day12>(const std::string& ans1, const std::string& ans2) {
    Day12 d(Day::day12, false, ans1, ans2);
    d.solve();
}
