#include <day.hpp>

#include <cctype>
#include <cassert>
#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>

#include <grid.hpp>

using std::cout;

struct Day20 : public Solution {
    using Solution::Solution;

    struct Portal {
        std::string name;
        vec2 a = vec2::null();
        vec2 b = vec2::null();
        std::set<vec2> pos;
        std::set<vec2> inner;
        std::set<vec2> outer;
    };

    const vec2 max {150, 150};
    Grid grid = cons_grid(max, ' ');

    inline char& at(const vec2& v){
        return grid[v.y][v.x];
    }

    static constexpr const char* START = "AA";
    static constexpr const char* END = "ZZ";

    std::map<std::string, Portal> portals;
    std::map<vec2, std::string> pos_portals;

    Portal cons_portal(vec2 name_start, vec2 name_end, vec2 port_to) {
        auto name = std::string(1,at(name_start)) + at(name_end);
        Portal p {name, port_to};
        p.pos.insert(name_start);
        p.pos.insert(name_end);
        return p;
    }


    void prepare(std::istream&& is) override {
        std::string line;

        int row_idx{};
        while(std::getline(is, line)) {
            grid[row_idx++] = Row{line.begin(), line.end()};
        }
        vec2 upper_left;
        vec2 lower_right;
        for(auto r = 0; r < grid.size()-1; r++) {
            for(auto c = 0; c < grid[0].size()-1; c++) {

                    if(r >0 && c >0 && at({c,r}) == ' ' && at({c,r-1}) == '#' && at({c-1,r}) == '#') {
                        upper_left = {c,r};
                        std::cout << upper_left << "\n";
                    }
                    if(r >0 && c >0 && at({c,r}) == ' ' && at({c,r+1}) == '#' && at({c+1,r}) == '#') {
                        lower_right = {c,r};
                        std::cout << lower_right << "\n";
                    }

                if(std::isalpha(at({c,r}))) {
                    if(std::isalpha(at({c+1, r}))) {
                        auto port_to = vec2::null();

                        if(c>0 && at({c-1, r}) == '.') port_to = {c-1,r};
                        else if(c<grid[0].size()-1 && at({c+2, r}) == '.') port_to = {c+2,r};

                        auto p = cons_portal({c,r}, {c+1,r}, port_to);
                        if(portals.find(p.name) == portals.end()) portals[p.name] = p;
                        else {
                            portals[p.name].b = p.a;
                            for(auto position : p.pos) {
                                portals[p.name].pos.insert(position);
                            }
                        }
                        for(auto pos : p.pos) {
                            pos_portals[pos] = p.name;
                        }
                    }
                    if(std::isalpha(at({c, r+1}))) {

                        auto port_to = vec2::null();
                        if(r>0 && at({c, r-1}) == '.') port_to = {c,r-1};
                        else if(r<grid.size()-1 && at({c, r+2}) == '.') port_to = {c,r+2};

                        auto p = cons_portal({c,r}, {c,r+1}, port_to);
                        if(portals.find(p.name) == portals.end()) portals[p.name] = p;
                        else {
                            portals[p.name].b = p.a;
                            for(auto position : p.pos) portals[p.name].pos.insert(position);
                        }
                        for(auto pos : p.pos) {
                            pos_portals[pos] = p.name;
                        }
                    }
                }

            }
        }
        for(auto& [name,p] : portals) {
            for(auto pos : p.pos) {
                if(pos <= lower_right && pos >= upper_left) {
                    p.inner.insert(pos);
                }
                else {
                    p.outer.insert(pos);
                }
            }
        }
    }

    std::string get_portal_name(vec2 pos) {
        return pos_portals[pos];
    }


    auto neighbours(const vec2& a, const Grid& grid) {
        std::vector<vec2> to_consider;
        if(a.x > 0) {
            to_consider.emplace_back(a.x-1, a.y);
        }
        if(a.x < grid[0].size()-1) {
            to_consider.emplace_back(a.x+1, a.y);
        }
        if(a.y > 0) {
            to_consider.emplace_back(a.x, a.y-1);
        }
        if(a.y < grid.size()-1) {
            to_consider.emplace_back(a.x, a.y+1);
        }

        std::vector<vec2> ret;
        for(auto v : to_consider) {
            if(at(v) != '.' && !std::isalpha(at(v))) {
                continue;
            }
            if(pos_portals[v] == START) {
                continue;
            }
            if(pos_portals[v] == END) {
                ret.push_back(v);
                continue;
            }
            if(auto name = get_portal_name(v); !name.empty()) {
                ret.push_back(portals[name].b);
                ret.push_back(portals[name].a);
            } else {
                ret.push_back(v);
            }
        }

        return ret;
    }


    std::string part1() override { 
        auto start = portals[START].a;
        start.y;
        auto [dist, path] = shortest_path(start, portals[END].a, grid, [this](const vec2& pos, const Grid& g){ return neighbours(pos, g); }, [](const auto&, const auto&) { return 0; } );
        return std::to_string(dist);
    }


    using Dist = int;
    using Layer = int;
    using Portals = std::string;

    struct State {
        vec2 pos;
        Layer layer{};
        Dist dist{};
        std::vector<std::string> portals_seen;
    };

    inline auto neighbours2(const vec2& a, const Grid& grid) {
        static std::map<vec2, std::vector<std::pair<vec2,std::string>>> cache;
        if(cache.find(a) != cache.end()) {
            return cache[a];
        }

        std::vector<vec2> to_consider;
        if(a.x > 0) {
            to_consider.emplace_back(a.x-1, a.y);
        }
        if(a.x < grid[0].size()-1) {
            to_consider.emplace_back(a.x+1, a.y);
        }
        if(a.y > 0) {
            to_consider.emplace_back(a.x, a.y-1);
        }
        if(a.y < grid.size()-1) {
            to_consider.emplace_back(a.x, a.y+1);
        }

        std::vector<std::pair<vec2,std::string>> ret;
        for(auto v : to_consider) {
            if(pos_portals[v] == START || at(v) != '.' && !std::isalpha(at(v))) {
                continue;
            }
            else if(pos_portals[v] == END) {
                ret.push_back({v,""});
                continue;
            }
            else if(auto name = get_portal_name(v); !name.empty()) {
                if(v.dist(portals[name].a) < v.dist(portals[name].b)) ret.push_back({portals[name].b, name});
                else ret.push_back({portals[name].a, name});
            } else {
                ret.push_back({v, ""});
            }
        }
        cache[a] = ret;
        return cache[a];
    }


    template<typename Container>
    std::string join(const Container& c) {
        std::string ret;
        for(auto i : c) {
            ret += i + ", ";
        }
        return ret;
    }

    int dist(const vec2& pos, const std::set<vec2>& vecs) {
        int dist{};
        for(const auto& p : vecs) {
            dist += p.dist(pos);
        }
        return dist;
    }

    int shortest_path2(vec2 start, vec2 end) {
        std::queue<State> queue;
        queue.push(State{start, Layer(0), Dist(0)});

        std::map<vec2, std::map<Layer, Dist>> dists;
        static std::map<vec2, std::map<std::string, bool>> is_inner_cache;

        while(!queue.empty()) {

            auto [pos, layer, cur_dist, seen_portals] = queue.front(); queue.pop();

            for(auto [cur_neighbor, portal_name] : neighbours2(pos, grid)) {

                auto new_layer = layer;
                if(!portal_name.empty()) {
                    if(is_inner_cache[cur_neighbor].find(portal_name) == is_inner_cache[cur_neighbor].end()) {
                        is_inner_cache[cur_neighbor][portal_name] = 
                        (dist(cur_neighbor, portals[portal_name].inner) < dist(cur_neighbor, portals[portal_name].outer));
                    }

                    if(is_inner_cache[cur_neighbor][portal_name]) {
                        --new_layer;
                    }
                    else {
                        ++new_layer;
                    }
                    if(new_layer < 0 || std::abs(new_layer) > portals.size()) continue;
                }

                if(auto it = dists[cur_neighbor].find(new_layer); it == dists[cur_neighbor].end() 
                || it->second > dists[pos][layer]+1) {

                    auto neighbor_dist = dists[cur_neighbor][new_layer] = dists[pos][layer]+1;
                    auto neighbors_portals = seen_portals;

                    if(!portal_name.empty()) {
                        neighbors_portals.push_back(portal_name);
                        std::sort(neighbors_portals.begin(), neighbors_portals.end());
                    }
                    queue.push({cur_neighbor, new_layer, neighbor_dist, std::move(neighbors_portals)});
                }
            }
        }
        return dists[end][0];
    }

    std::string part2() override { 
        auto start = portals[START].a;
        auto dist = shortest_path2(start, portals[END].a);
        return std::to_string(dist);
    }
};

template<>
void solve<Day::day20>(const std::string& ans1, const std::string& ans2) {
    Day20 d(Day::day20, false, ans1, ans2);
    d.solve();
}
