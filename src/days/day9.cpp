#include <day.hpp>

#include "string_utils.hpp"
#include "int_code.hpp"

struct Day9 : public Solution {
    using Solution::Solution;
    Program master_program;
    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    std::string part1() override { 
        CPU cpu;
        auto local = master_program;
        cpu.in_.push_back(1);
        cpu.run(local);
        return std::to_string(cpu.out_);
    }
    std::string part2() override { 
        CPU cpu;
        auto local = master_program;
        cpu.in_.push_back(2);
        cpu.run(local);
        return std::to_string(cpu.out_);
    }
};

template<>
void solve<Day::day9>(const std::string& ans1, const std::string& ans2) {
    Day9 d(Day::day9, false, ans1, ans2);
    d.solve();
}
