#include <day.hpp>

#include <algorithm>
#include <map>
#include <queue>
#include <set>
#include <stack>

#include "int_code.hpp"
#include "grid.hpp"

struct Day15 : public Solution {
    using Solution::Solution;

    Program master_program;
    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    enum Dir {
        North = 1,
        South,
        West,
        East,

        END
    };

    Dir opposite(Dir d) {
        if(d == North) return South;
        else if(d == South) return North;
        else if(d == East) return West;
        else if(d == West) return East;
        assert(false && "Invalid value provided");
        return North;
    }

    auto move (vec2 coord, Dir where) {
        if(where == South) coord.y-=1;
        else if(where == North) coord.y+=1;
        else if(where == East) coord.x+=1;
        else if(where == West) coord.x-=1;
        return coord;
    }

    const vec2 max = {100, 100};
    const vec2 start_coord{max.x/2, max.y/2};

    Grid grid = cons_grid(max, ' ');
    vec2 oxygen;

    std::vector<vec2> neighbours(const vec2& cell, const Grid& g) {
        auto x = cell.x;
        auto y = cell.y;
        std::vector<vec2> ret;
        if(x > 0 && g[y][x-1] != '#') ret.emplace_back(x-1, y);
        if(x < g[0].size() && g[y][x+1] != '#') ret.emplace_back(x+1, y);
        if(y > 0 && g[y-1][x] != '#') ret.emplace_back(x, y-1);
        if(y < g.size() && g[y+1][x] != '#') ret.emplace_back(x, y+1);
        return ret;
    }

    std::string part1() override { 
        enum Status {
            Wall,
            Moved, 
            Oxygen
        };



        auto local = master_program;
        Dir last_moved_dir;
        vec2 coord = start_coord;
        int commands = 0;

        CPU cpu;
        InterruptReason reason = cpu.run(local);
        std::set<vec2> seen;
        std::stack<Dir> breadcrumbs;

        while(reason != InterruptReason::Halt && commands != 5000) {

            if(reason == InterruptReason::Input) {
                commands++;
                bool added = {};
                for(auto dir : std::vector<Dir>{South, East, West, North}) {
                    if(!seen.count(move(coord, dir))) {
                        last_moved_dir = dir;
                        cpu.in_.push_back(last_moved_dir);
                        breadcrumbs.push(last_moved_dir);
                        added = true;
                        break;
                    }
                }
                if(!added) { 
                    assert(!breadcrumbs.empty());

                    auto dir = opposite(breadcrumbs.top()); breadcrumbs.pop();
                    last_moved_dir = dir;
                    cpu.in_.push_back(dir);
                }
            }
            else if(reason == InterruptReason::Output) {
                if(cpu.out_ == Wall) {
                    breadcrumbs.pop();
                    auto t = move(coord, last_moved_dir);
                    grid[t.y][t.x] = '#';
                    seen.insert(t);
                }
                else if(cpu.out_ == Moved) {
                    coord = move(coord, last_moved_dir);
                    assert(coord.x >= 0 && coord.x < 100 && coord.y >= 0 && coord.y < 100);
                    seen.insert(coord);
                } else if(cpu.out_ == Oxygen) {
                    coord = move(coord, last_moved_dir);
                    grid[coord.y][coord.x] = '@';
                    seen.clear();
                    oxygen = coord;
                }
            }
            reason = cpu.run(local);
        }
        auto [dist, path] = shortest_path({start_coord.x,start_coord.y}, oxygen, grid, [this](const vec2& cell, const Grid& g){
            return neighbours(cell, g);
        });
        return std::to_string(dist);
    }

    std::string part2() override { 
        grid[oxygen.y][oxygen.x] = 'O';
        std::set<vec2> seen;
        std::deque<vec2> Q {
            oxygen
        };
        int steps= -1;
        int how_many = 1;
        while(!Q.empty()) {
            auto c = how_many;
            how_many = 0;
            while(c--) {
                auto cell = Q.front(); Q.pop_front();
                if(seen.count(cell)) continue;
                seen.insert(cell);
                for(auto n : neighbours(cell, grid)) {
                    if(seen.count(n)) continue;
                    grid[n.y][n.x] = 'O';
                    Q.push_back(n);
                    ++how_many;
                }
            }
            ++steps;
        }
        return std::to_string(steps);
    }
};

template<>
void solve<Day::day15>(const std::string& ans1, const std::string& ans2) {
    Day15 d(Day::day15, false, ans1, ans2);
    d.solve();
}
