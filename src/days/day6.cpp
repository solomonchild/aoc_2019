#include <day.hpp>

#include <cassert>
#include <memory>
#include <vector>
#include <deque>
#include <set>
#include <unordered_map>

#include "string_utils.hpp"

    struct Node {
        std::string name;
        std::vector<Node*> children;
        Node* parent;

        int calculate_orbits() const {
            int sum = parent?1:0;
            if(parent) {
                sum += parent->calculate_orbits();
            }
            return sum;
        }
    };

    struct Day6 : public Solution {
        using Solution::Solution;
        std::unordered_map<std::string, std::unique_ptr<Node>> registry;
        Node* root = nullptr;

        void prepare(std::istream&& is) override {
            std::string line;
            while(std::getline(is, line)) {
                auto vec = split<std::string>(line, ")");
                auto name = vec[1];
                auto parent = vec[0];
                if(registry.find(parent) == registry.end()) {
                    registry[parent] = std::unique_ptr<Node>(new Node{parent});
                }
                if(auto it = registry.find(name); it == registry.end()) {
                    auto n = std::unique_ptr<Node>(new Node{name,{}, registry[parent].get()});
                    registry[parent]->children.push_back(n.get());
                    registry[name] = std::move(n);
                }
                else {
                    assert(!registry[name]->parent);
                    registry[name]->parent = registry[parent].get();
                    registry[parent]->children.push_back(it->second.get());
                }
        }
        for(const auto&[name, node] : registry) {
            if(!node->parent) {
                root = node.get();
                break;
            }
        }
    }
    std::string part1() override { 
        int num{};
        Node* root;
        auto sum = 0;
        auto count=0;
        for(const auto& [name,node] : registry) {
            sum += node->calculate_orbits();
        }
        return std::to_string(sum);
    }
    std::string part2() override { 

        struct Pair {Node* node{}; int steps{};};

        std::deque<Pair> Q {{registry["SAN"].get(), 0}};
        std::set<Node*> seen;
        while(!Q.empty()) {
            auto [node, steps] = Q.front(); Q.pop_front();
            seen.insert(node);

            if(node->name == "YOU") {
                return std::to_string(steps-2);
            }

            ++steps;

            if(node->parent && !seen.count(node->parent)) {
                Q.push_back({node->parent, steps});
            }
            for(auto c : node->children) {
                if(!seen.count(c)) Q.push_back({c, steps});
            }
        }
        return "";
    }
};

template<>
void solve<Day::day6>(const std::string& ans1, const std::string& ans2) {
    Day6 d(Day::day6, false, ans1, ans2);
    d.solve();
}