#include <day.hpp>

#include <array>
#include <iostream>
#include <utility>

#include <int_code.hpp>

using std::cout;

struct Day23 : public Solution {
    using Solution::Solution;

    Program master;
    void prepare(std::istream&& is) override {
        master = parse(std::move(is));
    }

    std::string part1() override { 
        std::array<CPU, 50> cpus;
        std::array<Program, 50> programs;

        for(auto& program : programs) {
            program = master;
        }
        for(auto cpu_idx = 0; cpu_idx < cpus.size(); ++cpu_idx) {
            cpus[cpu_idx].in_.push_back(cpu_idx);
        }
        bool run = true;
        for(;run;) {
            for(auto cpu_idx = 0; cpu_idx < cpus.size(); ++cpu_idx) {
                auto& cpu = cpus[cpu_idx];
                auto& program = programs[cpu_idx];
                auto reason = cpu.run(program);
                if(reason == InterruptReason::Output) {
                    auto address = cpu.out_;

                    reason = cpu.run(program);
                    assert(reason == InterruptReason::Output);
                    auto x = cpu.out_;

                    reason = cpu.run(program);
                    assert(reason == InterruptReason::Output);
                    auto y = cpu.out_;
                    if(address == 255) return std::to_string(y);
                    cpus[address].in_.push_back(x);
                    cpus[address].in_.push_back(y);
                }
                else if(reason == InterruptReason::Input) {
                    cpu.in_.push_back(-1);
                }
                if(reason == InterruptReason::InvalidInstruction) {
                    run = false;
                    break;
                } 
                assert(reason != InterruptReason::Halt);
            }
        }
        return Solution::part1();
    }
    bool all_empty(const std::array<CPU,50>& cpus) {
        return std::all_of(cpus.begin(), cpus.end(), [](const auto& cpu){
            return !cpu.in_.empty() && cpu.in_.front() == -1;
        }); 
    }

    std::string part2() override {
        std::array<CPU, 50> cpus;
        std::array<Program, 50> programs;

        for(auto& program : programs) { program = master; }
        for(auto cpu_idx = 0; cpu_idx < cpus.size(); ++cpu_idx) { cpus[cpu_idx].in_.push_back(cpu_idx); }
        bool run = true;
        struct {
            long long x{};
            long long y{};
        } nat_packet;
        auto y_sent_to_0 = -1;
        for(; run;) {

            if(all_empty(cpus)) {
                    if(nat_packet.y == y_sent_to_0) {
                        return std::to_string(nat_packet.y);
                    }
                    cpus[0].in_.push_back(nat_packet.x);
                    cpus[0].in_.push_back(nat_packet.y);
                    y_sent_to_0 = nat_packet.y;
            }

            for(auto cpu_idx = 0; cpu_idx < cpus.size(); ++cpu_idx) {
                auto& cpu = cpus[cpu_idx];
                auto& program = programs[cpu_idx];
                auto reason = cpu.run(program);
                if(reason == InterruptReason::Output) {

                    auto address = cpu.out_;
                    reason = cpu.run(program);
                    assert(reason == InterruptReason::Output);
                    auto x = cpu.out_;

                    reason = cpu.run(program);
                    assert(reason == InterruptReason::Output);
                    auto y = cpu.out_;
                    if(address == 255) {
                        nat_packet = {x, y};
                    } else {
                        cpus[address].in_.push_back(x);
                        cpus[address].in_.push_back(y);
                    }

                } else if(reason == InterruptReason::Input) {
                    cpu.in_.push_back(-1);
                }
                if(reason == InterruptReason::InvalidInstruction) {
                    run = false;
                    break;
                }
                assert(reason != InterruptReason::Halt);
            }
        }
        return {};
    }
};

template<>
void solve<Day::day23>(const std::string& ans1, const std::string& ans2) {
    Day23 d(Day::day23, false, ans1, ans2);
    d.solve();
}
