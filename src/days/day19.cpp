#include <day.hpp>

#include <int_code.hpp>
#include <grid.hpp>

struct Day19 : public Solution {
    using Solution::Solution;

    Program master_program;
    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }
    // char& at(const vec2& v) {
    //     assert(v.y < grid.size() && v.x < grid[0].size());
    //     return grid[v.y][v.x];
    // }


    inline bool is_beam(vec2 v) {
        if(v.x < 0 || v.y < 0) return {};
        CPU cpu;
        auto local = master_program;
        auto reason = cpu.run(local);
        assert(reason == InterruptReason::Input);
        cpu.in_.push_back(v.x);

        reason = cpu.run(local);
        assert(reason == InterruptReason::Input);
        cpu.in_.push_back(v.y);

        reason = cpu.run(local);
        assert(reason == InterruptReason::Output);
        return (cpu.out_ == 1);
    }

    std::string part1() override { 
        int total{};
        vec2 max{50,50};
        vec2 start = {0, 0};
        for(auto i = start.y; i < max.y; ++i) {
            for(auto j = start.x; j < max.x; ++j) {
                if(is_beam({j, i})) {
                    total++;

                }
            }
        }
        
        return std::to_string(total);
    }

    std::string part2() override { 
        int total{};
        vec2 max{1500,1500};
        int prev_col = {};
        for(auto i = 50; i < max.y; ++i) {
            for(auto j = prev_col; j < max.x; ++j) {
                if(is_beam({j, i})) {
                    prev_col = j;
                    if(is_beam({j+99, i-99})) {
                        return std::to_string(j*10000+(i-99));
                    }
                    break;
                }
            }
        }
        return "-1";
    }
};

template<>
void solve<Day::day19>(const std::string& ans1, const std::string& ans2) {
    Day19 d(Day::day19, false, ans1, ans2);
    d.solve();
}
