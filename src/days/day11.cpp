#include <day.hpp>

#include <complex>
#include <set>
#include <sstream>

#include "int_code.hpp"
#include "grid.hpp"

struct Day11 : public Solution {
    using Solution::Solution;

    Program program;
    void prepare(std::istream&& is) override {
        program = parse(std::move(is));
    }

    struct Robot : public CPU {
        InterruptReason run(Program& p) override{
            for (;; ++ip_) {
                OpCode opc(p.code[ip_]);
                if(auto it = handlers.find(opc.type); it == handlers.end())  {
                    std::cerr << "Program is malformed, no such op code " << static_cast<int>(opc.type) << "\n";
                    return {};
                }
                else if (!it->second(p, opc.modes, in_, out_)) 
                    return {};
                if(opc.type == OpCodeType::Output) {
                    ++ip_;
                    return InterruptReason::Output;
                }
            }
        }
    };

    using Direction = std::complex<int>;
    static auto get_direction_char(Direction dir) {
            if(dir.imag() == 1) return 'v';
            else if(dir.imag() == -1) return '^';
            else if(dir.real() == 1) return '>';
            else if(dir.real() == -1) return '<';
            return '?';
    }

    auto run_robot(vec2 grid_size = {100, 100}, bool start_black = true, bool should_print = false) {

        auto grid = cons_grid(grid_size, '.');
        vec2 cur{grid_size.x/2,grid_size.y/2};
        Direction dir{0,1};

        std::set<vec2> seen;
        std::set<vec2> white;
        if(!start_black) white.insert(cur);

        auto local_program = program;
        Robot cpu;
        while(true) {
            grid[cur.y][cur.x] = get_direction_char(dir);
            cpu.in_.push_back(white.count(cur)?1:0);
            assert(cpu.in_.size() == 1);

            if(cpu.run(local_program) == InterruptReason::Halt) {
                break;
            }

            assert(cpu.out_ == 0 || cpu.out_ == 1);
            if(cpu.out_ == 1) white.insert(cur);
            else white.erase(cur);

            seen.insert(cur);
            if(cpu.run(local_program) == InterruptReason::Halt) break;

            if(should_print) print(grid, true, false);
            assert(cpu.out_ == 0 || cpu.out_ == 1);
            char to_put = white.count(cur)?'#':'.';
            grid[cur.y][cur.x] = to_put;

            if(cpu.out_==0) dir *= std::complex{0, 1};  
            else dir *= std::complex{0,-1}; 

            cur.x += dir.real();
            cur.y -= dir.imag();
        }
        return std::pair{std::to_string(seen.size()), grid};
    } 
    std::string part1() override { 
        return run_robot().first;
    }

    std::string part2() override { 
        std::stringstream ss;
        print(run_robot({100, 100}, false).second, false, false, ss);
        return ss.str();
    }
};

template<>
void solve<Day::day11>(const std::string& ans1, const std::string& ans2) {
    Day11 d(Day::day11, false, ans1, ans2);
    d.solve();
}
