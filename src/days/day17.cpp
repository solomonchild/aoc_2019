#include <day.hpp>

#include <int_code.hpp>
#include <grid.hpp>

struct Day17 : public Solution {
    using Solution::Solution;

    Program master_program;
    void prepare(std::istream&& is) override {
        master_program = parse(std::move(is));
    }

    auto neighbours(const vec2& a, const Grid& grid) {
        std::vector<vec2> ret;
        if(a.x > 0) ret.emplace_back(a.x-1, a.y);
        if(a.x < grid[0].size()-1) ret.emplace_back(a.x+1, a.y);

        if(a.y > 0) ret.emplace_back(a.x, a.y-1);
        if(a.y < grid.size()-1) ret.emplace_back(a.x, a.y+1);
        return ret;
    }

    vec2 robot_loc;
    std::vector<vec2> scaffolds;

    const vec2 max {50, 50};
    Grid grid = cons_grid(max, ' ');

    std::string part1() override { 

        CPU cpu;
        auto local = master_program;
        auto reason = cpu.run(local);
        vec2 cur = {0, 0};
        while(reason != InterruptReason::Halt) {
            if(reason == InterruptReason::Output) {
                if(cpu.out_ == 10) {
                    cur.x = 0;
                    ++cur.y;
                } else {
                    if(cpu.out_ == '^') {
                        robot_loc = cur;
                    }
                    grid[cur.y][cur.x++] = char(cpu.out_);
                }
            }
            reason = cpu.run(local);
        }
        int total = 0;
        for(auto row = 0; row < grid.size(); ++row) {
            for(auto col = 0; col < grid[0].size(); ++col) {
                if(grid[row][col] != '#') continue;
                if(auto ns = neighbours({col, row}, grid); std::count_if(ns.begin(), ns.end(), [this](const auto v){ return grid[v.y][v.x] == '#'; }) == 4) {
                    grid[row][col] = 'O';
                    total += row*col;
                    scaffolds.emplace_back(col, row);
                }
            }
        }
        return std::to_string(total);
    }

    std::vector<std::string> str{
    };

    void zz(std::string input, std::set<vec2> seen) {

    }

    std::string part2() override { 
        std::ios::sync_with_stdio(false);
        auto local = master_program;
        assert(local.code[0] == 1);
        local.code[0] = 2;

        CPU cpu;
        std::ifstream is("../inputs/day17.in");
        assert(is);
        std::string line;

        std::getline(is, line);
        auto routine = line + "\xA";

        std::getline(is, line);
        auto A = line + "\xA";

        std::getline(is, line);
        auto B = line + "\xA";

        std::getline(is, line);
        auto C = line + "\xA";

        std::getline(is, line);
        auto feed = line + "\xA" ;

        for(auto c : routine)
            cpu.in_.push_back(c);

        for(auto c : A)
            cpu.in_.push_back(c);
        for(auto c : B)
            cpu.in_.push_back(c);
        for(auto c : C)
            cpu.in_.push_back(c);

        for(auto c : feed)
            cpu.in_.push_back(c);

        auto reason = cpu.run(local);
        vec2 cur = {0,0};
        while(reason != InterruptReason::Halt) {
            if(reason == InterruptReason::Output) {
                std::cout << char(cpu.out_);
            }
            reason = cpu.run(local); 
            if(reason != InterruptReason::Output)
            std::cerr << "Reason: " <<  int(reason) << "\n";
        }

        std::cout << cpu.out_ << "\n";
        return std::to_string(cpu.out_);
    }
};

template<>
void solve<Day::day17>(const std::string& ans1, const std::string& ans2) {
    Day17 d(Day::day17, false, ans1, ans2);
    d.solve();
}
