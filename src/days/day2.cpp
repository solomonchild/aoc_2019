#include <day.hpp>

#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "string_utils.hpp"
#include "int_code.hpp"

struct Day2 : public Solution {
    using Solution::Solution;

    Program program;
    void prepare(std::istream&& is) override {
        program = parse(std::move(is));
    }

    std::string part1() override { 
        auto local_program = program;
        CPU cpu;
        local_program.code[1]=12;
        local_program.code[2]=2;
        cpu.run(local_program);
        return std::to_string(local_program.code[0]);
    }

    std::string part2() override { 
        CPU cpu;
        for(auto i = 0; i <= 99; ++i) {
            for (auto j = 0; j <= 99; ++j) {
                cpu.reset();
                auto local_program = program;
                local_program.code[1] = i;
                local_program.code[2] = j;
                cpu.run(local_program);
                if(local_program.code[0] == 19690720) {
                    return std::to_string(100*i+j);
                }
            }
        }
        return "Nothing found";
    }
};

template<>
void solve<Day::day2>(const std::string& ans1, const std::string& ans2) {
    Day2 d(Day::day2, false, ans1, ans2);
    d.solve();
}
