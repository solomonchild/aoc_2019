#include "day.hpp"

#include <cassert>
#include "timer.hpp"

static std::ifstream open_fs(Day day) {
    const auto day_num = static_cast<int>(day);
    std::ifstream fs("../inputs/day" + std::to_string(day_num) + ".txt");
    assert(fs && "Could not open a file");
    return fs;
}

void Solution::solve() {
    using std::cout;
    const auto day = static_cast<int>(day_);
    auto test = [this, day](auto &&f, bool is_first = false, long long elapsed_before = 0LL) {
        auto [ans, elapsed] = TimeTask([=]() {
            if(pass_istream_to_parts_) 
                return f(open_fs(day_));
            else 
                return f();
        }).go();

        cout << (is_first ? "Part1: " : "Part2: ") << ans << '\n';
        if(!ans1_.empty() && !ans2_.empty()) {
            const std::string& to_comp = is_first?ans1_:ans2_;
            if(ans != to_comp) {
                std::cerr << "!!!!FAILED!!!! Expected: \"" << to_comp << "\", got: \"" << ans << "\"\n";
                assert(false);
            }
        }
        cout << "Elapsed: " << (elapsed + elapsed_before) << "ms\n\n";
    };

    cout << "-----Day" << day << "-----\n";
    long long preparation_time = 0LL;
    if(!pass_istream_to_parts_) {
        preparation_time = TimeTask([=](){
            prepare(open_fs(day_));
            return 0;
        }).go().second;
        cout << "Preparation time: " << preparation_time << "ms\n";
    }
    test([this](auto... params) { return part1(params...); }, true, preparation_time);
    test([this](auto... params) { return part2(params...); }, false, preparation_time);
    cout << "\n";
}