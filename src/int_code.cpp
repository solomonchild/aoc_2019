#include "int_code.hpp"

#include <cassert>

Program parse(std::istream &&is) {
    std::string line;
    Program program;
    while (std::getline(is, line)) {
        auto res = split<long long>(line, ",");
        program.code.insert(program.code.end(), res.begin(), res.end());
    }
    return program;
}

auto CPU::get_mode (int& mode_idx, const Modes& modes) -> Mode {
    if(mode_idx >= modes.size()) return Mode::Position;
    else return modes[mode_idx++];
}

static auto& access(int ip, Program& p) {
    if(ip >= p.code.size()) {
        p.code.resize(ip+1);
    }
    return p.code[ip];
}

auto CPU::fetch(Program& program, int& mode_idx, const Modes& modes) -> long long& {
    auto mode = get_mode(mode_idx, modes);
    ++ip_;
    access(ip_, program);
    if (mode == Mode::Immediate) {
        return access(ip_, program);
    }
    else if(mode == Mode::Position) {
        return access(program.code[ip_], program);
    }
    else {
        return access(rel_base_ + program.code[ip_], program);
    }
}

void CPU::reset() {
    ip_ = 0;
    in_.clear();
    out_ = 0;
    rel_base_ = 0;
}

InterruptReason CPU::run(Program& p) {
    for (;; ++ip_) {
        OpCode opc(p.code[ip_]);
        if(auto it = handlers.find(opc.type); it == handlers.end())  {
            std::cerr << "Program is malformed, no such op code " << static_cast<int>(opc.type) << "\n";
            return InterruptReason::InvalidInstruction;
        }

        if(opc.type == OpCodeType::InputToPos && in_.empty()) {
            return InterruptReason::Input;
        }
        if(!handlers[opc.type](p, opc.modes, in_, out_))  {
            return InterruptReason::Halt;
        }
        if(opc.type == OpCodeType::Output) {
            ++ip_;
            return InterruptReason::Output;
        }
    }
}