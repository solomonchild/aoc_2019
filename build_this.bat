cls
IF NOT EXIST build (
    md build
)
pushd build
cmake -G Ninja ../ || exit /b 1
cmake --build . || exit /b 1
aoc.exe
popd
