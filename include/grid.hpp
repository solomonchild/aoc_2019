#pragma once

#include <cmath>
#include <iomanip>
#include <functional>
#include <iostream>
#include <map>
#include <cmath>
#include <queue>
#include <set>
#include <sstream>
#include <thread>
#include <vector>
//TODO: indexing by operator[]
//TODO: make grid resizeable
//TODO: vec3
//TODO: 3d grid


//TODO: std::hash
struct vec2 
{
    int x{}, y{};
    vec2(int x, int y) 
    : x(x), y(y) {}
    vec2() = default;

    int dist(const vec2& other) const {
        auto dx = other.x - x;
        auto dy = other.y - y;
        return std::sqrt(dx*dx + dy*dy);
    }

    vec2 operator-(const vec2& other) const {
        return {x-other.x, y-other.y};
    }
    inline bool operator==(const vec2& o) const {
        return x==o.x && y==o.y;
    }
    inline bool operator!=(const vec2& o) const {
        return !(*this==o);
    }
    inline bool operator<(const vec2& o) const {
        if(x == o.x) return y < o.y;
        return x < o.x;
    }

    inline bool operator>(const vec2& o) const {
        if(x == o.x) return y > o.y;
        return x > o.x;
    }

    inline bool operator<=(const vec2& o) const {
        return x <= o.x && y <= o.y;
    }

    inline bool operator>=(const vec2& o) const {
        return x >= o.x && y >= o.y;
    }

    friend std::ostream &operator<<(std::ostream &os, const vec2 &vec) {
        os << "[" << vec.x << ", " << vec.y << "]";
        return os;
    }
    static vec2 null() {
        return {-1,-1};
    }
};

static inline auto manhattan(const vec2& a, const vec2& b) {
    return std::abs(b.x-a.x) + std::abs(b.y-a.y);
};

static inline auto dist(vec2 one, vec2 two) {
    auto o = one.x-two.x;
    auto t = one.y-two.y;
    return std::sqrt(o*o+t*t);
}

using Row = std::vector<char>;
using Grid = std::vector<Row>;

static inline Grid cons_grid(vec2 max, char fill = ' ') {
    Grid grid(max.y);
    for(auto& r : grid) {
        r.resize(max.x, fill);
    }
    return grid;
}


static inline void print_top_numbers(const Grid& grid, std::ostream& os) {
    const auto height = grid.size();
    const auto width = grid[0].size();
    std::string indent = " ";
    if(height>10) indent += " ";
    if (width > 10) {
        for (auto i = 0; i < width; ++i) {
            if (i == 0)
                os << indent;
            if (i >= 10) {
                os << (i / 10);
            }
            else os << " ";
        }
        os << "\n";
    }
    for(auto i = 0; i < width; ++i) {
        if(i == 0) os << indent;
        os << (i%10);
    }
    os << "\n";
}

static inline void print(const Grid& grid, bool clear = false, bool print_nums = false, std::ostream& os = std::cout) {
    std::stringstream ss;
    if(print_nums) print_top_numbers(grid, ss);
    const auto height = grid.size();
    int row_n{};
    auto w = 1;
    if(height>10)++w;

    for(auto& r : grid) {
        if(print_nums)
        ss << std::setw(w) << std::setfill(' ') << row_n++;
        for(auto c : r) ss << c;
        ss << "\n";
    }
    if(clear) system("cls");
    os << ss.str();
}

//For debugging
static inline void ray(vec2 from, vec2 to, Grid& grid) {
    auto sleep = []() {
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
    };
    auto diff_x = to.x - from.x;
    auto diff_y = to.y - from.y;
    if(std::abs(diff_x) > std::abs(diff_y)) {
        auto t = diff_y/float(diff_x);
        auto sign = (diff_x<0)?-1:1;
        for(auto i = 0; i <= std::abs(diff_x); ++i) {
            auto p = i*sign;
            auto prev = std::exchange(grid[from.y+p*t][from.x+p], '+');
            print(grid, true);
            sleep();
            grid[from.y+p*t][from.x+p] = prev;
        }
    }
    else {
        auto t = diff_x/float(diff_y);
        auto sign = (diff_y<0)?-1:1;
        for(auto i = 0; i <= std::abs(diff_y); ++i) {
            auto p = i*sign;
            auto prev = std::exchange(grid[from.y+p][from.x+p*t], '+');
            print(grid, true);
            sleep();
            grid[from.y+p][from.x+p*t] = prev;
        }
    }
}

using HeuristicsF = std::function<int(const vec2& v1, const vec2& v2)>;
static inline std::pair<int, std::vector<vec2>> shortest_path(vec2 a, vec2 dest, const Grid& grid, std::function<std::vector<vec2>(const vec2&, const Grid&)> neighbours, HeuristicsF heur = manhattan) {
    std::set<vec2> seen;
    std::map<vec2, vec2> from;
    std::priority_queue<
        std::pair<vec2, int>, 
        std::vector<std::pair<vec2,int>>, 
        std::function<bool(const std::pair<vec2,int>&, const std::pair<vec2,int>&)>
    > to_get  ([](const auto& a, const auto& b){
            return a.second > b.second;
    });

    to_get.push({a, heur(a, dest)});
    std::map<vec2, int> dist_map {
        {a, 0}
    };

    std::vector<vec2> path;
    while(!to_get.empty()) {
        auto [cell, d] = to_get.top(); to_get.pop();

        if(!seen.insert(cell).second) continue;
        if(cell == dest) {
            auto n = from[dest];
            path.push_back(dest);
            while(n != a) {
                path.push_back(n);
                n = from[n];
            }
            break;
        }

        for(auto n : neighbours(cell, grid)) {
            if(seen.count(n)) continue;

            if(auto it = dist_map.find(n); it == dist_map.end()) {
                dist_map[n] = dist_map[cell] + 1;
                from[n] = cell;
            }
            else {
                if(dist_map[cell]+1 < it->second) {
                    it->second = dist_map[cell]+1;
                    from[n] = cell;
                }
            }
            to_get.push({n, heur(n, dest)});
        }

    }
    return {dist_map[dest], {path.rbegin(), path.rend()}};
}
