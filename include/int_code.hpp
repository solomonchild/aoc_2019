#pragma once

#include <cassert>
#include <vector>
#include <deque>
#include <unordered_map>
#include <functional>
#include <istream>
#include <iostream>

#include "string_utils.hpp"

enum class InterruptReason {
    Halt,
    Output,
    Input,
    InvalidInstruction
};

enum class OpCodeType {
    Sum = 1,
    Mul = 2,
    InputToPos = 3,
    Output = 4,

    JIT = 5,
    JIF = 6,
    LT = 7,
    EQ = 8,
    RelBaseOffset = 9,

    Halt = 99,
    Undefined = 999
};

enum class Mode {
    Position,
    Immediate,
    Relative
};

using Modes = std::vector<Mode>;
using Code = std::vector<long long>;
using Input = std::deque<long long>;
using Output = long long;

struct OpCode {
    OpCodeType type = OpCodeType::Undefined;
    Modes modes;
    OpCode(int i) {
        type = static_cast<OpCodeType>(i % 100);
        i /= 100;
        while (i) {
            modes.push_back(static_cast<Mode>(i % 10));
            i /= 10;
        }
    }
};

struct Program {
    Code code;
};

Program parse(std::istream &&is);

struct CPU {

    std::unordered_map<OpCodeType, std::function<bool(Program&, const Modes&, Input&, Output& )>> handlers {
        {OpCodeType::Sum, [this](Program& program, const Modes& modes, Input&, Output&){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            auto& where = fetch(program, mode_idx, modes);
            where = first+second;
            return true;
        }},
        {OpCodeType::Mul, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            auto& where = fetch(program, mode_idx, modes);
            where = first*second;
            return true;
        }},
        {OpCodeType::InputToPos, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto& where = fetch(program, mode_idx, modes);
            assert(!inputs.empty());
            where = inputs.front();
            inputs.pop_front();
            return true;
        }},
        {OpCodeType::Output, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            output = first;
            return true;
        }},
        {OpCodeType::JIT, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            if(first) {
                ip_ = second - 1;
            }
            return true;
        }},
        {OpCodeType::JIF, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            if(!first) {
                ip_ = second - 1;
            }
            return true;
        }},
        {OpCodeType::LT, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            auto& where = fetch(program, mode_idx, modes);
            if(first<second) where = 1;
            else where = 0;
            return true;
        }},
        {OpCodeType::EQ, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            auto second = fetch(program, mode_idx, modes);
            auto& where = fetch(program, mode_idx, modes);
            if(first==second) where = 1;
            else where = 0;
            return true;
        }},
        {OpCodeType::RelBaseOffset, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            auto mode_idx = 0;
            auto first = fetch(program, mode_idx, modes);
            rel_base_ += first;
            return true;
        }},
        {OpCodeType::Halt, [this](Program& program, const Modes& modes, Input& inputs, Output& output){
            return false;
        }}
    };

    auto get_mode (int& mode_idx, const Modes& modes) -> Mode; 
    auto fetch (Program& program, int& mode_idx, const Modes& modes) -> long long&;
    void reset(); 
    virtual ~CPU() = default;
    virtual InterruptReason run(Program& p);

    Input in_{};
    Output out_{};
    int ip_{};
    int rel_base_{};
};