#pragma once

#include <chrono>

template <typename F>
class TimeTask {
public:
    TimeTask(F &&f)
    : f_(std::move(f))
    {
    }

    auto go() {
      auto start = std::chrono::system_clock::now();
      auto ans = f_();
      auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count();
      return std::pair(ans, elapsed);
    }

private:
    F f_;
};