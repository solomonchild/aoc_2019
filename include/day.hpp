#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

enum class Day {
    day1=1,
    day2,
    day3,
    day4,
    day5,
    day6,
    day7,
    day8,
    day9,
    day10,
    day11,
    day12,
    day13,
    day14,
    day15,
    day16,
    day17,
    day18,
    day19,
    day20,
    day21,
    day22,
    day23,
    day24,
    day25
};

struct Solution {
    Solution(Day day, bool pass_istream_to_parts = false, const std::string& ans1="", const std::string& ans2="")
      : day_(day),
        ans1_(ans1),
        ans2_(ans2),
        pass_istream_to_parts_(pass_istream_to_parts)
    {}
    virtual std::string part1(std::istream& is) { return {}; }
    virtual std::string part2(std::istream& is) { return {}; }

    virtual std::string part1() { return {}; }
    virtual std::string part2() { return {}; }

    virtual void prepare(std::istream&& is) {}


    virtual void solve();

private:
    std::string ans1_;
    std::string ans2_;
    Day day_;
    bool pass_istream_to_parts_;
};

template<Day D>
void solve(const std::string& a="", const std::string& b="");
