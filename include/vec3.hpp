#pragma once

#include <cassert>
#include <iomanip>
#include <iostream>

struct vec3
{
    int x{}, y{}, z{};
    int& operator[](int i) {
        if(i == 0) return x;
        if(i == 1) return y;
        if(i == 2) return z;
        assert(false); return x;
    }
    bool operator==(const vec3& v) const {
        return x == v.x && y == v.y && z == v.z;
    }
    bool operator<(const vec3& v) const {
        if(x==v.x) {
            if(y==v.y) return z<v.z;
            return y<v.y;
        }
        return x < v.x;
    }
    friend std::ostream &operator<<(std::ostream &os, const vec3 &v) {
        os << "[" << std::setw(3) << std::setfill(' ') << v.x << ", " << std::setw(3) << std::setfill(' ') << v.y << ", " << std::setw(3) << std::setfill(' ') << v.z << "]";
        return os;
    }
};