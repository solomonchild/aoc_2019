#pragma once

#include <sstream>
#include <string>
#include <vector>

template<typename T>
static std::vector<T> split(const std::string& str, const std::string& delims = " ") {
    std::vector<T> res;
    size_t idx = 0;
    std::stringstream ss;
    while(auto end = str.find_first_of(delims, idx)) {
        auto sub = str.substr(idx, end-idx);

        if(!sub.empty()) {
            ss.str(str.substr(idx, end-idx));
            ss.seekg(0);
            T t{};
            ss >> t;
            res.push_back(t);
        }
        idx = end + 1;
        if(end == std::string::npos) break;
    }
    return res;
}

template<>
std::vector<std::string> split(const std::string& str, const std::string& delims) {
    std::vector<std::string> res;
    size_t idx = 0;
    std::stringstream ss;
    while(auto end = str.find_first_of(delims, idx)) {
        auto sub = str.substr(idx, end-idx);

        if(!sub.empty()) {
            ss.str(str.substr(idx, end-idx));
            ss.seekg(0);
            std::string t{};
            t = ss.str();
            res.push_back(t);
        }
        idx = end + 1;
        if(end == std::string::npos) break;
    }
    return res;
}
static std::string strip(const std::string& str) {
    auto start = str.begin();
    if(str.empty()) return {};
    while(start != str.end() && *start == ' ') {
        ++start;
    }
    auto end = str.end()-1;
    while(end != str.begin() && *end == ' ') {
        --end;
    }
    return {start, end+1};
}