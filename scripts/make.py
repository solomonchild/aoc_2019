def gen_txt(fr, to):
    for day in range(fr, to+1):
        open(f"day{day}.txt", "w") 

def gen_sources(fr, to):
    for day in range(fr, to+1):
        with open(f"day{day}.cpp", "w") as f:
            f.write("#include <day.hpp>\n\n")
            f.write(f"struct Day{day} : public Solution {{\n")
            f.write(f"    using Solution::Solution;\n")

            f.write("    void prepare(std::istream&& is) override {\n")
            f.write("    }\n")

            f.write("    std::string part1() override { \n")
            f.write("        return Solution::part1();\n")
            f.write("    }\n")

            f.write("    std::string part2() override { \n")
            f.write("        return Solution::part2();\n")
            f.write("    }\n")
            f.write("};\n\n")

            f.write("template<>\n")

            f.write(f"void solve<Day::day{day}>(const std::string& ans1, const std::string& ans2) {{\n")
            f.write(f"    Day{day} d(Day::day{day}, false, ans1, ans2);\n")
            f.write(f"    d.solve();\n")
            f.write("}\n")
